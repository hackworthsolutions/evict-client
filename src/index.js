import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './screens/Root';
import configureStore from './store';
import './index.css';

const initialState = {};

const store = configureStore( initialState );
const history = syncHistoryWithStore( browserHistory, store );

ReactDOM.render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);
