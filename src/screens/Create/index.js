import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createRoom } from '../../actions/rooms';

import { Card, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';
import { Input } from 'react-toolbox/lib/input';
import { Switch } from 'react-toolbox/lib/switch';
import { Button } from 'react-toolbox/lib/button';
import style from './style.scss';

const actionCreators = {
  push,
  createRoom,
};

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      public: true,
      p2p: false,
    };

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleChange.bind(this, 'name');
    this.handlePublicChange = this.handleChange.bind(this, 'public');
    this.handleP2PChange = this.handleChange.bind(this, 'p2p');
  }

  componentDidMount() {
    if(this.inputRef) {
      // React-toolbox magic
      this.inputRef.refs.wrappedInstance.refs.input.focus();
    }
  }

  handleKeyPress({ charCode }) {
    if(charCode === 13) {
      this.handleSubmit();
    }
  }

  handleSubmit = () => {
    this.props.createRoom(this.state)
    .then((room) => this.props.push(`/room/${ room._id }`))
  };

  handleChange = (field, value) => {
    this.setState({...this.state, [field]: value});
  };

  render() {
    const {
      push,
    } = this.props;

    return (
      <div className={style.cardContainer}>
        <Card className={style.card}>
          <CardTitle
            title="Create and configure your room"
            subtitle="Choose visibility"
          />
          <CardText>
            <Input
              type='text'
              label='Room name'
              name='name'
              value={this.state.name}
              onChange={this.handleNameChange}
              maxLength={16}
              ref={(el) => this.inputRef = el}
              onKeyPress={this.handleKeyPress}
            />
            <div>
              <Switch
                checked={this.state.public}
                label="Public"
                onChange={this.handlePublicChange}
              />
              <Switch
                checked={this.state.p2p}
                label="P2P"
                onChange={this.handleP2PChange}
              />
            </div>
          </CardText>
          <CardActions className={style.actions}>
            <Button onClick={this.handleSubmit}  icon="add" label="CREATE" raised accent />
            <Button onClick={() => push('/')} icon="clear" label="CANCEL" raised />
          </CardActions>
        </Card>
      </div>
    );
  }
}

Create.propTypes = {
  push: PropTypes.func,
  createRoom: PropTypes.func,
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Create);

