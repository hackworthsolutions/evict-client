import React, { PropTypes, Component } from 'react';
import { Provider } from 'react-redux';
import Routes from '../../routes'

class Root extends Component {
  render() {
    const {
      store,
      history,
    } = this.props;
    return (
      <Provider store={store}>
        <Routes history={history} />
      </Provider>
    );
  }
}

Root.propTypes = {
  store: PropTypes.any,
  history: PropTypes.any,
};

export default Root;

