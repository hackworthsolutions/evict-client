import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AppBar } from 'react-toolbox/lib/app_bar';

import { push } from 'react-router-redux';

import 'react-toolbox/lib/commons.scss';
import style from './style.scss';

const actionCreators = {
  push,
};

class App extends Component {
  render() {
    const {
      children,
      push,
    } = this.props;

    return (
      <div className={style.container}>
        <AppBar
          title="Easy Video Chat"
          leftIcon="videocam"
          onLeftIconClick={() => push('/')}
        />
        <div>
          { children }
        </div>
      </div>
    );
  }
}

App.propTypes = {
  push: PropTypes.func,
  children: PropTypes.any,
};

export default connect(
  state => state,
    dispatch  => bindActionCreators(actionCreators, dispatch)
)(App);
