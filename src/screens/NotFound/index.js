import React, { Component } from 'react';
import { Card, CardTitle } from 'react-toolbox/lib/card';

class NotFound extends Component {
  render() {
    return (
      <Card style={{width: '350px'}}>
        <CardTitle
          title="Page not found!"
          subtitle="Nothing to see here"
        />
      </Card>
    );
  }
}

NotFound.propTypes = {
};

export default NotFound;


