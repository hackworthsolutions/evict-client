import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  connectRoom,
  roomConnected,
  streamSubscribed,
  streamAdded,
  streamRemoved,
  createLocalStream,
  updateLocalStream,
  updateRemoteStream,
  toggleScreen,
} from '../../actions/erizo';

import VideoPlayer from '../../components/VideoPlayer';
import ForeverAlone from '../../assets/img/forever_alone.jpg';

import { Card, CardTitle, CardActions } from 'react-toolbox/lib/card';
import { Input } from 'react-toolbox/lib/input';
import { Button } from 'react-toolbox/lib/button';

import classnames from 'classnames';
import style from './style.scss';

import grid from '../../utils/grid';

const BARSIZE=60;

const actionCreators = {
  connectRoom,
  roomConnected,
  streamSubscribed,
  streamAdded,
  streamRemoved,
  createLocalStream,
  updateLocalStream,
  updateRemoteStream,
  toggleScreen,
};

class Room extends Component {
  constructor(props){
    super(props);

    this.state = {
      name: '',
      isFolded: false,
      height: window.innerHeight - BARSIZE,
      width: window.innerWidth,
    };

    this.updateDimensions = this.updateDimensions.bind(this);
    this.createLocalStream = this.createLocalStream.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.connectRoom = this.connectRoom.bind(this);
    this.onRoomConnected = this.onRoomConnected.bind(this);
    this.onStreamSubscribed = this.onStreamSubscribed.bind(this);
    this.onStreamAdded = this.onStreamAdded.bind(this);
    this.onStreamRemoved = this.onStreamRemoved.bind(this);
  }

  componentWillMount() {
    this.createLocalStream();
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    if(this.inputRef) {
      // React-toolbox magic
      this.inputRef.refs.wrappedInstance.refs.input.focus();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.erizoRoom && nextProps.erizoRoom) {
      // I just connected!
      const room = nextProps.erizoRoom;

      room.addEventListener("room-connected", this.onRoomConnected);
      room.addEventListener("stream-subscribed", this.onStreamSubscribed);
      room.addEventListener("stream-added", this.onStreamAdded);
      room.addEventListener("stream-removed", this.onStreamRemoved);
      room.connect();
      console.log('Connected to ', nextProps.erizoRoom);
    }
  }

  updateDimensions() {
    this.setState({width: window.innerWidth, height: window.innerHeight - BARSIZE });
  }

  createLocalStream() {
    this.props.createLocalStream();
  }

  handleKeyPress({ charCode }) {
    if(charCode === 13) {
      this.connectRoom();
    }
  }

  connectRoom() {
    this.props.connectRoom(this.props.params.id, this.state.name || 'Unidentified User');
  }

  onRoomConnected(event){
    this.props.roomConnected(event.streams);
    setTimeout(() => this.props.updateLocalStream({ name: this.state.name }), 500);
  }

  onStreamSubscribed(event){
    this.props.streamSubscribed(event.stream);
  }

  onStreamAdded(event){
    this.props.streamAdded(event.stream);
  }

  onStreamRemoved(event){
    this.props.streamRemoved(event.stream);
  }

  render() {
    const {
      name,
      isFolded,
    } = this.state;

    const {
      erizoRoom,
      erizoStreams = [],
      erizoStreamsLastUpdates = {},
      myErizoStream,
      updateLocalStream,
      updateRemoteStream,
      toggleScreen,
    } = this.props;

    const otherErizoStreams = erizoStreams
      .filter(stream => !myErizoStream || stream.getID() !== myErizoStream.getID())
      .sort((a, b) => {
        if(a.getID() > b.getID()) return -1;
        else if(a.getID() < b.getID()) return 1;
        else return 0;
      });

    const sizes = grid(otherErizoStreams.length, window.innerWidth, window.innerHeight - BARSIZE);

    const isConnected = !!erizoRoom;
    const isLocalStream = !!myErizoStream;

    const className = {
      localStream: classnames(style.localStream, {
        [style.topRight]: isLocalStream && isConnected,
        [style.topRightUnfolded]: isLocalStream && isConnected && !isFolded,
        [style.topRightFolded]: isLocalStream && isConnected && isFolded,
        [style.fullScreen]: isLocalStream && !isConnected,
      }),
      remoteStreams: classnames(style.remoteStreams, {
        [style.fullScreen]: isLocalStream && isConnected,
      }),
      remoteStream: classnames(style.remoteStream),
      identityCardContainer: classnames(style.identityCardContainer, {
        [style.bottomCenterStripe]: isLocalStream && !isConnected,
        [style.fullScreen]: !isLocalStream && !isConnected,
      }),
    };

    const localWidth = isFolded ? 80 : 320;
    const localHeight = isFolded ? 60 : 240;

    return (
      <div className={style.container}>
        { isLocalStream ?
          <div
            className={className.localStream}
          >
            <VideoPlayer
              width={isConnected ? `${localWidth}px` : 'auto'}
              height={isConnected ? `${localHeight}px` : 'auto'}
              folded={isFolded}
              maxed={!isConnected}
              onToggleFold={() => this.setState({ isFolded: !isFolded })}
              onMuteAudio={() => {
                if(myErizoStream.stream) {
                  const audios = myErizoStream.stream.getAudioTracks();
                  if(audios && audios[0]) {
                    audios[0].enabled = !audios[0].enabled;
                    updateLocalStream(myErizoStream.getAttributes());
                  }
                }
              }}
              onToggleScreen={toggleScreen}
              stream={myErizoStream}
              lastUpdate={ erizoStreamsLastUpdates[myErizoStream.getID()] }
              local={true}
            />
          </div>
        : null }
        { !isConnected
        ?
          <div
            className={className.identityCardContainer}
          >
            <Card
              className={style.identityCard}
            >
              <CardTitle
                title="Identify yourself!"
                subtitle="Please type your name and click GO!"
              />
              <CardActions>
                <Input
                  type='text'
                  label='Your name'
                  name='name'
                  value={name}
                  onChange={name => this.setState({ name })}
                  ref={(el) => this.inputRef = el}
                  onKeyPress={this.handleKeyPress}
                />
                <Button onClick={this.connectRoom} label="GO" raised accent />
              </CardActions>
            </Card>
          </div>
        :
          <div
            className={className.remoteStreams}
          >
            { (otherErizoStreams.length > 0) ? otherErizoStreams.map((stream, index) => (
              <div
                key={stream.getID()}
                className={className.remoteStream}
                style={{
                  minWidth: `${sizes[index].w}px`,
                  maxWidth: `${sizes[index].w}px`,
                  minHeight: `${sizes[index].h}px`,
                  maxHeight: `${sizes[index].h}px`,
                }}
              >
                <VideoPlayer
                  width={`${sizes[index].w}px`}
                  height={`${sizes[index].h}px`}
                  maxWidth={`${sizes[index].w}px`}
                  stream={stream}
                  onMuteAudio={() => {
                    if(stream.stream) {
                      const audios = stream.stream.getAudioTracks();
                      if(audios && audios[0]) {
                        audios[0].enabled = !audios[0].enabled;
                        updateRemoteStream(stream);
                      }
                    }
                  }}
                  lastUpdate={ erizoStreamsLastUpdates[stream.getID()] }
                />
              </div>
              )) :
              <div className={style.aloneContainer}>
                <img
                  className={style.alone}
                  src={ForeverAlone}
                  alt={'Forever Alone'}
                />
              </div>}
          </div>
        }
      </div>
    );
  }
}

Room.propTypes = {
};

Room.propTypes = {
  push: PropTypes.func,
  connectRoom: PropTypes.func,
  roomConnected: PropTypes.func,
  streamSubscribed: PropTypes.func,
  streamAdded: PropTypes.func,
  streamRemoved: PropTypes.func,
  createLocalStream: PropTypes.func,
  updateLocalStream: PropTypes.func,
  updateRemoteStream: PropTypes.func,
  toggleScreen: PropTypes.func,
  erizoRoom: PropTypes.object,
  erizoStreams: PropTypes.array,
  erizoStreamsLastUpdates: PropTypes.object,
  myErizoStream: PropTypes.object,
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Room);
