import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { listRooms, deleteRoom } from '../../actions/rooms';
import config from '../../config';
import style from './style.scss';

import { Card, CardTitle, CardText } from 'react-toolbox/lib/card';
import { Button, IconButton } from 'react-toolbox/lib/button';
import { List, ListItem, ListSubHeader } from 'react-toolbox/lib/list';
import { Avatar } from 'react-toolbox/lib/avatar';

const actionCreators = {
  push,
  listRooms,
  deleteRoom,
};

const getInitials = name => {
  name.split(/\s+/g).reduce(function (previousValue, currentValue) {
    return previousValue + currentValue.charAt(0);
  }, "");
};

class ListRooms extends Component {
  componentWillMount() {
    this.props.listRooms();
  }

  render() {
    const {
      rooms = [],
      push,
      loading,
      deleteRoom,
    } = this.props;

    return (
      <div className={style.cardContainer}>
        <Card className={style.card}>
          <CardTitle
            title="List of rooms"
          />
          <CardText>
            { loading > 0 ?
              <div>
                <div>Loading rooms</div>
              </div>
            : rooms && rooms.length > 0 ?
              <div>
                <List selectable ripple>
                  <ListSubHeader caption="Public rooms" />
                  { rooms.map(room => (
                    <ListItem
                      key={room._id}
                      onClick={() => push(`/room/${room._id}`)}
                      avatar={
                        room.name
                        ? <Avatar
                            title={getInitials(room.name)}
                            style={{backgroundColor: 'plum'}}
                          />
                        : config.DEFAULT_ROOM_AVATAR
                      }
                      caption={`Room: ${ room.name }`}
                      legend={room.p2p ? '(p2p) ' : '(star)'}
                      rightIcon='redo'
                      rightActions={[ <IconButton icon='delete' onClick={() => deleteRoom(room._id) } /> ]}
                    />
                  ))}
                </List>
                <Button onClick={() => push('/create')} icon="add" floating accent />
              </div>
            :
              <div>
                <div>No rooms found</div>
                <Button onClick={() => push('/create')} icon="add" label="CREATE ONE" raised primary />
              </div>
            }
          </CardText>
        </Card>
      </div>
    );
  }
}

ListRooms.propTypes = {
  push: PropTypes.func,
  listRooms: PropTypes.func,
  deleteRoom: PropTypes.func,
  rooms: PropTypes.array,
  loading: PropTypes.number,
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(ListRooms);

