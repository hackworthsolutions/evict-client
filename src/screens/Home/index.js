import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { Card, CardTitle, CardActions } from 'react-toolbox/lib/card';
import {Button} from 'react-toolbox/lib/button';
import style from './style.scss';

const actionCreators = {
  push
};

class Home extends Component {
  render() {
    const { push } = this.props;
    return (
      <div className={style.cardContainer}>
        <Card className={style.card}>
          <CardTitle
            title="Welcome to EasyVIdeoChat"
            subtitle="You can start playing around"
          />
          <CardActions className={style.actions}>
            <Button onClick={() => push('/create')} icon="add" label="CREATE" raised accent/>
            <Button onClick={() => push('/list')} icon="forward" label="JOIN" raised />
          </CardActions>
        </Card>
      </div>
    );
  }
}

Home.propTypes = {
  push: PropTypes.func
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Home);

