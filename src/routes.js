import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { Router } from 'react-router';
import App from './screens/App';
import Home from './screens/Home';
import Create from './screens/Create';
import List from './screens/List';
import Room from './screens/Room';
import NotFound from './screens/NotFound';

const Routes = ({ history }) => (
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="create" component={Create} />
      <Route path="list" component={List} />
      <Route path="room/:id" component={Room} />
      <Route status={404} path="*" component={NotFound} />
    </Route>
  </Router>
);

export default Routes;

