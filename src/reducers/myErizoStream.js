export default function(state = null, action) {
  switch(action.type) {
    case 'STREAM_ERIZO_LOCAL':
      return action.payload;
    default:
      return state;
  }
}
