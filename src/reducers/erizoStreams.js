export default function(state = [], action) {
  switch(action.type) {
    case 'STREAMS_ERIZO_SUBSCRIBED':
      return action.payload;
    case 'STREAM_ERIZO_ADDED':
      return state.concat(action.payload);
    case 'STREAM_ERIZO_REMOVED':
      return state.filter(stream => stream.getID() !== action.payload.getID());
    case 'STREAM_ERIZO_UPDATED':
      return state
        .filter(stream => stream.getID() !== action.payload.getID())
        .concat(action.payload);
    default:
      return state;
  }
}
