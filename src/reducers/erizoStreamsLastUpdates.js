export default function(state = {}, action) {
  switch(action.type) {
    case 'STREAMS_ERIZO_SUBSCRIBED':
      return action.payload
        .map(stream => ({ id: stream.getID(), timestamp: action.timestamps }))
        .reduce((obj, next) => { obj[next.id] = next.timestamp; return obj }, {});
    case 'STREAM_ERIZO_ADDED':
    case 'STREAM_ERIZO_REMOVED':
    case 'STREAM_ERIZO_UPDATED':
      return {
        ...state,
        [action.payload.getID()]: action.timestamp
      }
    default:
      return state;
  }
}
