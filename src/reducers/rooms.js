export default function(state = [], action) {
  switch(action.type) {
    case 'LIST_ROOM_REQUEST':
      return [];
    case 'LIST_ROOM_DONE':
      return action.payload;
    case 'DELETE_ROOM_DONE':
      return state.filter(room => room._id !== action.payload);
    default:
      return state;
  }
}
