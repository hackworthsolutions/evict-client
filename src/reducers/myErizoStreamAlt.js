export default function(state = null, action) {
  switch(action.type) {
    case 'STREAM_ERIZO_LOCAL':
      return action.alt;
    default:
      return state;
  }
}
