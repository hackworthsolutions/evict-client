import erizoRoom from './erizoRoom';
import erizoStreams from './erizoStreams';
import erizoStreamsLastUpdates from './erizoStreamsLastUpdates';
import loading from './loading';
import myErizoStream from './myErizoStream';
import myErizoStreamAlt from './myErizoStreamAlt';
import rooms from './rooms';

export default {
  erizoRoom,
  erizoStreams,
  erizoStreamsLastUpdates,
  loading,
  myErizoStream,
  myErizoStreamAlt,
  rooms,
};

