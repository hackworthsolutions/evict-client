const requestRE = new RegExp('_REQUEST$');
const doneRE = new RegExp('_(DONE|CANCEL)$');

export default function(state = 0, action) {
  if(action.type.match(requestRE)) return state+1;
  if(action.type.match(doneRE)) return state >= 1 ? state-1 : 0;
  return state;
}
