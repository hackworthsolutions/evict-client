export default function(state = null, action) {
  switch(action.type) {
    case 'CONNECT_ERIZO_REQUEST':
      return null;
    case 'CONNECT_ERIZO_DONE':
      return action.payload;
    default:
      return state;
  }
}
