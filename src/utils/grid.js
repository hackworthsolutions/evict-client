const MAGIC = {
  2: 1,
  5: 2,
};

export default function(n, w, h) {
  if(n === 0) return [];

  const MARGIN = 7;
  const tracks = MAGIC[n] || Math.ceil(Math.sqrt(n));
  const perTrack = Math.ceil(n / tracks);
  const inLastTrack = n - ( (tracks - 1) * perTrack );

  return Array((tracks - 1) * perTrack).fill(0)
    .map(() => ({ w: Math.floor(w/perTrack)-MARGIN, h: Math.floor(h/tracks)-MARGIN }))
    .concat(Array(inLastTrack).fill(0)
      .map(() => ({ w: Math.floor(w/inLastTrack) - MARGIN, h: Math.floor(h/tracks)-MARGIN }))
    );
}

