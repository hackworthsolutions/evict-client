import fetch from 'isomorphic-fetch';
import Promise from 'promise-polyfill';

function toJSON(response) {
  if(!response) {
    return Promise.reject({ code: 400, text: 'Bad Request', response });
  }
  if(!response.ok) {
    return Promise.reject({ code: response.status, text: response.statusText, response });
  }
  return response.json().catch(() => ({})); // Catch empty responses
}

function request(method, url, body, headers = {}){
  let options = {
    method,
    headers: Object.assign({}, body ? { 'Content-Type': 'application/json' } : {}, headers),
  };

  if(body) options.body = JSON.stringify(body);

  return fetch(url, options)
  .then(toJSON)
  .catch(error => Promise.reject(error));
}

export default {
  get:    (...args) => request('GET', ...args),
  post:   (...args) => request('POST', ...args),
  put:    (...args) => request('PUT', ...args),
  patch:  (...args) => request('PATCH', ...args),
  delete: (...args) => request('DELETE', ...args),
}
