import http from '../utils/http';

const request = (verb) => ({
  type: `${verb}_ROOM_REQUEST`
});

const done = (verb, payload) => ({
  type: `${verb}_ROOM_DONE`,
  payload
});

export const listRooms = () => (dispatch, getState, config) => {
  const verb = 'LIST';
  dispatch(request(verb));
  return http.get(`${config.API_URL}rooms`)
    .then(rooms => dispatch(done(verb, rooms)));
}
  
export const createRoom = (payload) => (dispatch, getState, config) => {
  const verb = 'CREATE';
  dispatch(request(verb));
  return http.post(`${config.API_URL}rooms`, payload)
  .then(room => {
    dispatch(done(verb, room))
    return room;
  });
}

export const deleteRoom = (id) => (dispatch, getState, config) => {
  const verb = 'DELETE';
  dispatch(request(verb));
  return http.delete(`${config.API_URL}rooms/${id}`)
  .then(() => {
    dispatch(done(verb, id))
    return id;
  });
}

