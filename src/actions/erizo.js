import http from '../utils/http';
const Erizo = window.Erizo;

const request = (verb) => ({
  type: `${verb}_ERIZO_REQUEST`
});

const done = (verb, payload) => ({
  type: `${verb}_ERIZO_DONE`,
  payload
});

const connected = (payload, timestamp = Date.now()) => ({
  type: 'STREAMS_ERIZO_SUBSCRIBED',
  payload,
  timestamp,
});

const addStream = (payload, timestamp = Date.now()) => ({
  type: 'STREAM_ERIZO_ADDED',
  payload,
  timestamp,
});

const removeStream = (payload, timestamp = Date.now()) => ({
  type: 'STREAM_ERIZO_REMOVED',
  payload,
  timestamp,
});

const updateStream = (payload, timestamp = Date.now()) => ({
  type: 'STREAM_ERIZO_UPDATED',
  payload,
  timestamp,
});

const setLocal = (payload, alt, timestamp = Date.now()) => ({
  type: 'STREAM_ERIZO_LOCAL',
  payload,
  alt,
  timestamp,
});

export const connectRoom = (id, name) => (dispatch, getState, config) => {
  const verb = 'CONNECT';
  dispatch(request(verb));
  return http.post(`${config.API_URL}rooms/${id}/token`, { name, role: 'presenter' })
    .then(token => {
      const room = Erizo.Room({ token });
      dispatch(done(verb, room))
      return room;
    });
}

export const roomConnected = (streams) => (dispatch, getState, config) => {
  // TODO: Check localStreams
  const room = getState().erizoRoom;
  const localStream = getState().myErizoStream;
  room.publish(localStream);
  for (let stream of streams){
    room.subscribe(stream);
    stream.addEventListener('bandwidth-alert', (event) => {
      console.log('Bandwidth Alert', event.msg, event.bandwidth);
    });
  }
  dispatch(connected(streams));
}

export const streamSubscribed = (stream) => (dispatch, getState, config) => {
  console.log('Stream subscribed', stream.getID());
  dispatch(updateStream(stream));
	stream.addEventListener('stream-attributes-update', (event) => {
    console.log('Stream Attributes Update', event);
    dispatch(updateStream(event.stream));
  });
}

export const streamAdded = (stream) => (dispatch, getState, config) => {
  const room = getState().erizoRoom;
  room.subscribe(stream);
  stream.addEventListener('bandwidth-alert', (event) => {
    console.log('Bandwidth Alert', event.msg, event.bandwidth);
  });
  dispatch(addStream(stream));
}

export const streamRemoved = (stream) => (dispatch, getState, config) => {
  dispatch(removeStream(stream));
}

export const createLocalStream = () => (dispatch, getState, config) => {
  const stream = Erizo.Stream({
    audio: true,
    video: true,
    data: true,
    videoSize: [ ...config.MIN_VIDEO_RES, ...config.MAX_VIDEO_RES ] ,
    attributes: { name: 'TBD' }
  });
  stream.addEventListener('access-accepted', (event) => {
    console.log('Access accepted', event);
    dispatch(setLocal(stream, null));
  });
  stream.addEventListener('access-denied', (event) => {
    console.log('Access denied', event);
  });
  stream.init();
}

export const toggleScreen = () => (dispatch, getState, config) => {
  const room = getState().erizoRoom;
  const main = getState().myErizoStream;
  const alt = getState().myErizoStreamAlt;
  if(alt) {
    dispatch(setLocal(alt, main));
  } else {
    const stream = Erizo.Stream({
      screen: true,
      audio: true,
      data: true,
      extensionId: config.EXTENSION_ID,
      videoSize: [ ...config.MIN_SCREEN_RES, ...config.MAX_SCREEN_RES ] ,
      attributes: { name: (main.getAttributes() || {}).name || '' + ' (screen)' }
    });
    stream.addEventListener('access-accepted', (event) => {
      console.log('Access accepted', event);
      dispatch(setLocal(stream, main));
      room.publish(stream);
    });
    stream.addEventListener('access-denied', (event) => {
      console.log('Access denied', event);
    });
    stream.init();
  }
}

export const updateLocalStream = (attrs) => (dispatch, getState, config) => {
  const stream = getState().myErizoStream;
  const alt = getState().myErizoStreamAlt;
  stream.setAttributes(attrs);
  dispatch(setLocal(stream, alt));
}

export const updateRemoteStream = (stream) => (dispatch, getState, config) => {
  dispatch(updateStream(stream));
}

