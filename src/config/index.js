export default {
  API_URL: 'http://localhost:4000/',
  DEFAULT_ROOM_AVATAR: 'https://dl.dropboxusercontent.com/u/2247264/assets/m.jpg',
  MIN_VIDEO_RES: [ 800, 600 ],
  MAX_VIDEO_RES: [ 1440, 1080 ],
  MIN_SCREEN_RES: [ 1024, 768 ],
  MAX_SCREEN_RES: [ 1440, 1080 ],
  EXTENSION_ID: 'bbamhhnpkijcggjmhmfafgfiokfhhedl',
}

