/* global window, webkitURL */
import React, { Component } from 'react';
import { Button } from 'react-toolbox';

const styles = {
  div: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  video: {
    minWidth: '100%',
    minHeight: '100%',
    position: 'absolute',
    top: '0px',
    left: '0px',
    objectFit: 'cover',
  },
  controls: {
    position: 'absolute',
    bottom: '0px',
    display: 'flex',
    justifyContent: 'space-around',
    paddingBottom: '15px',
    width: '75%',
  },
  control: {
    flex: '0 0 auto',
  },
  label: {
    position: 'absolute',
    bottom: '0px',
    width: '100%',
    height: '3.3em',
    lineHeight: '3.3em',
    color: '#fff',
    fontSize: '2em',
    fontWeight: 'bolder',
    backgroundColor: 'rgba(0, 0, 0, 0.4)'
  },
};

class VideoPlayer extends Component {

  shouldComponentUpdate(_new){
    const _old = this.props;

    return (
      _old.local !== _new.local ||
      _old.folded !== _new.folded ||
      !_old.stream || !_new.stream ||
      _old.lastUpdate !== _new.lastUpdate ||
      _old.height !== _new.height ||
      _old.width !== _new.width
    );
  }

  componentDidMount() {
    if(this.props.local && this.videoRef) {
      this.videoRef.volume = 0;
    }
  }

  render() {
    const {
      stream,
      local = false,
      folded = false,
      maxed = false,
      onToggleFold,
      onMuteAudio,
      onToggleScreen,
      height = 'auto',
      width = 'auto',
    } = this.props;

    const live = !!stream.stream;
    const streamUrl = live ? (window.URL || webkitURL).createObjectURL(stream.stream) : '';
    let name = '???';
    if(stream.getAttributes && stream.getAttributes()) {
      name = stream.getAttributes().name || name;
    }

    // This is MediaStream magic
    const isMuted = stream.stream &&
      stream.stream.getAudioTracks() &&
      stream.stream.getAudioTracks()[0] &&
      stream.stream.getAudioTracks()[0].enabled;

    return (
      <div style={{
        ...styles.div,
        height,
        width
      }}>
        <video
          style={{
            ...styles.video,
            height,
            width
          }}
          ref={(el) => this.videoRef = el}
          autoPlay="autoplay"
          src={streamUrl}
          onClick={() => {
            if(local && folded) {
              onToggleFold();
            } else if(!local) {
              onMuteAudio();
            }
          }}
        ></video>
        { local && !folded && !maxed ?
          <div style={styles.controls}>
            <Button
              style={styles.control}
              floating
              icon={'call_made'}
              onClick={onToggleFold}
            />
            <Button
              style={styles.control}
              floating
              accent={isMuted}
              icon={isMuted ? 'mic' : 'mic_off'}
              onClick={onMuteAudio}
            />
            <Button
              style={styles.control}
              floating
              accent={stream.hasScreen()}
              icon={stream.hasScreen() ? 'screen_share' : 'stop_screen_share'}
              onClick={onToggleScreen}
            />
          </div>
        : null }
        { !local ?
          <div style={styles.label}>
            { name }
            <span>
              &nbsp;
              &nbsp;
              &nbsp;
              <Button
                floating
                accent={isMuted}
                icon={isMuted ? 'volume_up' : 'volume_off'}
                onClick={onMuteAudio}
              />
            </span>
          </div>
        : null }
      </div>
    );
  }
}

export default VideoPlayer;

